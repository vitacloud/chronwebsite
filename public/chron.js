/**
 * Created by tacticurv on 04/11/17.
 */

showSignOut(false);

function onSignIn(googleUser) {
    const profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    showSignOut(true);
}

function signOut() {
    const auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
        showSignOut(false);
    });
}

function showSignOut(isVisible) {
    const x = document.getElementById("signout_button");
    if(isVisible){
        x.style.visibility = "visible";
    } else {
        x.style.visibility = "hidden";
    }
}